import os
import datetime
import sqlite3
import xml.etree.ElementTree as ET
import argparse
import gspread


class estaff_importer:
    def __init__(self, store):
        self.sw = {
            "vacancy": self.load_vacancy,
            "vacancy_instance": self.loadvacancy_instance,
            "candidate": self.loadCandidate,
            "division": self.loadDivisions,
            "event": self.loadEvent,
            "user": self.loadUsers,
        }
        self.db = store
        pass

    # выводит список изменившихся файлов базы естафф
    def getNewFiles(self, path, dt=None):
        filelist = []
        if dt == None:
            for root, dirs, files in os.walk(path):
                for file in files:
                    if file[-3:] == "xml":
                        p = os.path.join(root, file)
                        d = datetime.datetime.fromtimestamp(os.path.getmtime(p))
                        filelist.append([p, d])
        else:
            for root, dirs, files in os.walk(path):
                for file in files:
                    if file[-3:] == "xml":
                        p = os.path.join(root, file)
                        d = datetime.datetime.fromtimestamp(os.path.getmtime(p))
                        if d > dt:
                            filelist.append([p, d])
        return sorted(filelist, key=lambda file: file[1])

    # тут держим процедуры разбора xml и разнесению по разным процендурам загрузки
    def fileSwitcher(self, path):
        try:
            root = ET.parse(path)
            print(path, root.getroot().tag)
            func = self.sw.get(root.getroot().tag, None)
            if func is not None:
                func(xml=root.getroot())
        except:
            print('Ошибка обработки файла'+path)
            pass

    def load_vacancy(self, xml):
        vals = (
            xml.findall("./id")[0].text,
            xml.findall("./name")[0].text if len(xml.findall("./name")) > 0 else '',
            xml.findall("./code")[0].text if len(xml.findall("./code")) > 0 else '',
            xml.findall("./start_date")[0].text if len(xml.findall("./start_date")) > 0 else '',
            xml.findall("./work_end_date")[0].text if len(xml.findall("./work_end_date")) > 0 else '',
            xml.findall("./close_date")[0].text if len(xml.findall("./close_date")) > 0 else '',
            xml.findall("./state_id")[0].text if len(xml.findall("./state_id")) > 0 else '',
            xml.findall("./req_quantity")[0].text if len(xml.findall("./req_quantity")) > 0 else '',
            xml.findall("./recruit_phase_id")[0].text if len(xml.findall("./recruit_phase_id")) > 0 else '',
            xml.findall("./inet_uid")[0].text if len(xml.findall("./inet_uid")) > 0 else '',
            xml.findall("./rr_persons_desc")[0].text if len(xml.findall("./rr_persons_desc")) > 0 else '',
            xml.findall("./final_candidate_id")[0].text if len(xml.findall("./final_candidate_id")) > 0 else '',
            xml.findall("./priority_id")[0].text if len(xml.findall("./priority_id")) > 0 else '',
            xml.findall("./difficulty_level_id")[0].text if len(xml.findall("./difficulty_level_id")) > 0 else '',
            xml.findall("./staff_category_id")[0].text if len(xml.findall("./staff_category_id")) > 0 else '',
        )
        fields = (
            "id", "name", "code", "start_date",
            "work_end_date", "close_date",
            "state_id", "req_quantity",
            "recruit_phase_id", "inet_uid",
            "rr_persons_desc", "final_candidate_id",
            "priority_id",
            "difficulty_level_id",
            "staff_category_id",
        )
        self.db.insert_or_update("Vacancies", fields, vals)

    def loadEvent(self, xml):
        vals = (
            xml.findall("./id")[0].text,
            xml.findall("./type_id")[0].text if len(xml.findall("./type_id")) > 0 else '',
            xml.findall("./date")[0].text if len(xml.findall("./date")) > 0 else '',
            xml.findall("./candidate_id")[0].text if len(xml.findall("./candidate_id")) > 0 else '',
            xml.findall("./vacancy_id")[0].text if len(xml.findall("./vacancy_id")) > 0 else '',
            xml.findall("./user_id")[0].text if len(xml.findall("./user_id")) > 0 else '',
        )
        fields = (
            "id", "type_id", "date", "candidate_id",
            "vacancy_id", "user_id")
        self.db.insert_or_update("Events", fields, vals)

    def loadCandidate(self, xml):
        vals = (
            xml.findall("./id")[0].text,
            xml.findall("./fullname")[0].text if len(xml.findall("./fullname")) > 0 else '',
        )
        fields = (
            "id", "fullname",)
        self.db.insert_or_update("Candidates", fields, vals)

    def loadDivisions(self, xml):
        vals = (
            xml.findall("./id")[0].text,
            xml.findall("./name")[0].text if len(xml.findall("./name")) > 0 else '',
            xml.findall("./code")[0].text if len(xml.findall("./code")) > 0 else '',
        )
        fields = ("id", "name", "code")
        self.db.insert_or_update("Divisions", fields, vals)

    def loadvacancy_instance(self, xml):
        vals = (
            xml.findall("./id")[0].text,
            xml.findall("./vacancy_id")[0].text if len(xml.findall("./vacancy_id")) > 0 else '',
            xml.findall("./division_id")[0].text if len(xml.findall("./division_id")) > 0 else '',
            xml.findall("./name")[0].text if len(xml.findall("./name")) > 0 else '',
            xml.findall("./code")[0].text if len(xml.findall("./code")) > 0 else '',
            xml.findall("./start_date")[0].text if len(xml.findall("./start_date")) > 0 else '',
            xml.findall("./state_id")[0].text if len(xml.findall("./state_id")) > 0 else '',
            xml.findall("./recruit_phase_id")[0].text if len(xml.findall("./recruit_phase_id")) > 0 else '',
            xml.findall("./req_quantity")[0].text if len(xml.findall("./req_quantity")) > 0 else '',
            xml.findall("./final_candidate_id")[0].text if len(xml.findall("./final_candidate_id")) > 0 else '',
            xml.findall("./final_candidate_state_id")[0].text if len(
                xml.findall("./final_candidate_state_id")) > 0 else '',
            xml.findall("./is_active")[0].text if len(xml.findall("./is_active")) > 0 else '',
            xml.findall("./user_id")[0].text if len(xml.findall("./user_id")) > 0 else '',

        )
        fields = (
            "id", "vacancy_id", "division_id", "name",
            "code", "start_date",
            "state_id", "recruit_phase_id",
            "req_quantity", "final_candidate_id",
            "final_candidate_state_id", "is_active", "user_id")
        self.db.insert_or_update("vacancy_instance", fields, vals)

    def loadUsers(self, xml):
        vals = (
            xml.findall("./id")[0].text,
            xml.findall("./fullname")[0].text if len(xml.findall("./fullname")) > 0 else '',
        )
        fields = (
            "id", "fullname",)
        self.db.insert_or_update("Users", fields, vals)


class store:
    event_types = {
        'meeting': 'Встреча у клиента',
        'office_meeting': 'Встреча у нас',
        'note': 'Пометка',
        'vacancy_response': 'Отклик на вакансию',
        'found_with_ras': 'Найден роботом',
        'candidate_phone_call': 'Первый звонок',
        'failed': 'Отклонен рекрутером',
        'withdrawn': 'Кандидат не заинтересован',
        'peer_undecided': 'Кандидат думает',
        'invitation': 'Начальный email',
        'video_interview_poll': 'Видео-интервью',
        'phone_interview': 'Тел. интервью',
        'interview': 'Интервью',
        'group_interview_reg': 'Запись на групповое интервью',
        'group_interview_result': 'Результат группового интервью',
        'testing': 'Тестирование',
        'online_interview': 'Онлайн-интервью',
        'rr_resume_review': 'Резюме у заказчика',
        'rr_interview': ' Интервью с заказчиком',
        'rr_approve': 'Утверждение руководителями',
        'rr_poll': 'Оценка руководителями',
        'rr_interview2': '2-е интервью с заказчиком',
        'job_offer': 'Оффер',
        'candidate_form_request': 'Запрос анкеты',
        'security_check': 'Проверка СБ',
        'hire_paperwork': 'Оформление',
        'hire': 'Принят на работу',
        'probation_assessment': 'Оценка прохождения испытательного срока',
        'reject': 'Отклонен рекрутером',
        'rr_reject': 'Отклонен заказчиком',
        'self_reject': 'Самоотказ',
        'reserve': 'Кадровый резерв',
        'vacancy_close': 'Не прошел финальный отбор',
        'vacancy_cancel': 'Вакансия отменена',
        'dismiss': 'Уволен',
        'blacklist': '"Черный список"',
        'candidate_select': 'Отобран на вакансию',
        'card_update': 'Данные обновлены',
        'candidate_letter': 'Отправлено письмо кандидату',
        'client_letter': 'Отправлено письмо заказчику',
        'group_interview': 'Групповое интервью',
        'reserve_feelance': 'Фриланс-резерв',
        'from_landing': 'Пришел с лендинга',
        'comment': 'Комментарий',
        'chat_bot': 'Чат-бот',
        'event_type_1': 'Интервью с руководителем',
        'message_sent':'Отправлено письмо',
        'move2another_vacancy':'Перевод на другую вакансию',
    }
    vacancy_status = {
        'vacancy_submitted': 'Новая заявка',
        'vacancy_opened': 'В работе',
        'vacancy_closed': 'Закрыта',
        'vacancy_closed_by_rr': 'Закрыта заказчиком',
        'vacancy_suspended': 'Приостановлена',
        'vacancy_reopened': 'Возобновлена',
        'vacancy_cancelled': 'Отменена',
        'vacancy_wating_for_free_recruiter': 'Сверх суммарной загрузки',
        '': ''
    }
    vacancy_priority = {"1": "высокая", "2": "обычная", "3": "низкая", '': ''}
    staff_category = {'0': 'грейд 0', '1': 'грейд 1', '2': 'грейд 2', '3': 'грейд 3', '4': 'грейд 4', '5': 'грейд 5',
                      '6': 'грейд 6', '': ''}
    vacancy_difficulty_level = {'1': 'высокая', '2': 'средняя', '3': 'Простая', '': ''}

    def __init__(self, path='estaff_report.db'):
        self.connection = sqlite3.connect(path)
        self.cursor = self.connection.cursor()
        # Создаем таблицу Vacancies
        # Срочность вакансии	Сложность вакансии	Категория вакансии

        self.init()

    def init(self):
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS Vacancies (
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL,
            code TEXT NOT NULL,
            start_date TEXT,
            work_end_date TEXT,
            close_date  TEXT,
            state_id TEXT,
            recruit_phase_id TEXT,
            req_quantity int,
            inet_uid  TEXT,
            rr_persons_desc TEXT,
            final_candidate_id TEXT,
            priority_id text,
            difficulty_level_id text,
            staff_category_id text,
            FOREIGN KEY(final_candidate_id) REFERENCES Candidates(id)
            )
            ''')
        # Создаем таблицу Candidates
        self.cursor.execute('''
                    CREATE TABLE IF NOT EXISTS Candidates (
                    id TEXT PRIMARY KEY,
                    fullname TEXT NOT NULL
                    )
                    ''')
        # Создаем таблицу Users
        self.cursor.execute('''
                            CREATE TABLE IF NOT EXISTS Users (
                            id TEXT PRIMARY KEY,
                            fullname TEXT NOT NULL
                            )
                            ''')
        # Создаем таблицу Divisions
        self.cursor.execute('''     CREATE TABLE IF NOT EXISTS Divisions (
                                    id TEXT PRIMARY KEY,
                                    name TEXT NOT NULL,
                                    code TEXT                                    
                                    )
                                    ''')
        # Создаем таблицу Events
        self.cursor.execute('''     CREATE TABLE IF NOT EXISTS Events (
                                        id TEXT PRIMARY KEY,
                                        type_id TEXT NOT NULL,
                                        date TEXT NOT NULL,
                                        candidate_id TEXT,
                                        vacancy_id TEXT,
                                        user_id TEXT,
                                        FOREIGN KEY(user_id) REFERENCES Users(id),
                                        FOREIGN KEY(candidate_id) REFERENCES Candidates(id),
                                        FOREIGN KEY(vacancy_id) REFERENCES Vacancies(id)                                    
                                    )
                                            ''')
        self.cursor.execute('''     CREATE TABLE IF NOT EXISTS key_value (
                                                id TEXT PRIMARY KEY,
                                                value TEXT NOT NULL                             
                                            )
                                                    ''')
        # Создаем таблицу vacancy_instance
        self.cursor.execute('''     CREATE TABLE IF NOT EXISTS vacancy_instance (
                                            id TEXT PRIMARY KEY,
                                            vacancy_id TEXT NOT NULL,
                                            division_id TEXT,
                                            name TEXT NOT NULL,
                                            code TEXT NOT NULL,
                                            start_date TEXT, 
                                            state_id TEXT,
                                            recruit_phase_id TEXT,
                                            req_quantity int,
                                            final_candidate_id TEXT,
                                            final_candidate_state_id TEXT,
                                            is_active TEXT,
                                            user_id TEXT,
                                            FOREIGN KEY(user_id) REFERENCES Users(id),
                                            FOREIGN KEY(final_candidate_id) REFERENCES Candidates(id),
                                            FOREIGN KEY(vacancy_id) REFERENCES Vacancies(id),
                                            FOREIGN KEY(division_id) REFERENCES Divisions(id)               
                                            )
                                            ''')
        self.connection.commit()
        pass

    def insert_or_update(self, tbl, fields, vals):
        sql = 'insert into "' + tbl + '" (' + ",".join(fields) + ') values ' + str(vals)
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except sqlite3.IntegrityError as err:
            sql = f'UPDATE {tbl} SET '
            for i in range(1, len(fields)):
                sql += '"' + fields[i] + '"="' + vals[i] + '", '
            sql = sql[0:-2]
            sql += f' WHERE id="{vals[0]}"'
            print(err)
            self.cursor.execute(sql)
            self.connection.commit()
        except sqlite3.Error as err:
            print(err)

    def select(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def exit(self):
        self.connection.close()


class gshWrite():
    def __init__(self, key, gdoc, sheetname='report'):
        self.gc = gspread.service_account(filename=key)
        self.gs = self.gc.open_by_key(gdoc)
        self.ws = self.gs.worksheet(sheetname)
    def set_sheet(self,name='report'):
        self.ws = self.gs.worksheet(name)

    def read_range(self, range):
        return self.ws.acell(range).value

    def read_cell(self, row, coll):
        return self.ws.cell(row, coll).value

    def write(self, range, val):
        return self.ws.update(range_name=range, values=val)

    def bu(self, data):
        return self.ws.batch_update(data)

    def write_cell(self, row, coll, val):
        # self.ws.batch_update
        return self.ws.update_cell(row, coll, val)

    def append_row(self, val, indx):
        return self.ws.insert_row(values=val, index=indx, inherit_from_before=True)


def loader(args):
    s = store(path=args.DBPATH)
    ei = estaff_importer(store=s)
    last_update = s.select(sql="select value from key_value where id='last_update'")
    last_update = datetime.datetime.fromisoformat(last_update[0][0]) if len(last_update) > 0 else None
    print(last_update)
    for tst_ in ei.getNewFiles(path=args.dirname, dt=last_update):
        ei.fileSwitcher(tst_[0])
    s.insert_or_update(tbl="key_value", fields=["id", "value"], vals=("last_update", datetime.datetime.now().__str__()))
    last_update = s.select(sql="select value from key_value where id='last_update'")
    last_update = datetime.datetime.fromisoformat(last_update[0][0]) if len(last_update) > 0 else None
    print(last_update)

def report(args):
    gw = gshWrite(key=args.key, gdoc=args.gdoc)
    s = store(path=args.DBPATH)
    report_by_period(gw=gw,s=s,period=0)
    report_by_period(gw=gw, s=s, period=1)
    report_by_period(gw=gw, s=s, period=2)
    report_by_period(gw=gw, s=s, period=3)
    report_by_period(gw=gw, s=s, period=4)
def report_disable(args):
    gw = gshWrite(key=args.key, gdoc=args.gdoc)

    s = store(path=args.DBPATH)
    vacancy = s.select(sql='''select
                                v.name as "Vacancy", v.code, v.start_date, v.close_date, v.state_id,
                                v.inet_uid as "HH", v.rr_persons_desc as "Requestor", d.name as "Dep", 
                                v.id, u.fullname as "Recruter", c.fullname as "FinalCandidate", 
                                v.staff_category_id as 'StaffCat', v.difficulty_level_id as 'Difficulty',
                                v.priority_id as 'Priority'
                            from
                                Vacancies v
                            left join vacancy_instance vi on v.id=vi.vacancy_id
                            left join "Divisions" d on vi.division_id = d.id
                            left join "Users" u on u.id= vi.user_id
                            left join "Candidates" c on c.id= v.final_candidate_id
                            order by v.state_id,v.code desc ''')

    gw.ws.batch_clear(["A2:R200"])

    data = []
    formula = [[], []]
    r = 2
    for v in vacancy[:]:
        c = s.select(f'''select count(*) 
                            from (
                                select distinct e.candidate_id 
                                from "Events" e
                                where  e.vacancy_id = "{v[8]}"
                                and 
                                    )''')

        stat = s.select(f''' select e.type_id, count(e.id)  
                            from "Events" e
                            where  e.vacancy_id = "{v[8]}"
                            group by e.type_id
                        ''')
        st = "Статистика по событиям:\n"
        for row in stat:
            st += '\t' + s.event_types[row[0]] + ':' + str(row[1]) + ',\n'
        vacancy_response = s.select(f'''select distinct c.fullname 
                                    from "Events" e, "Candidates"  c
                                    where  
                                        e.vacancy_id =  "{v[8]}"
                                        and c.id = e.candidate_id
                                        and e.type_id in ('vacancy_response',  'found_with_ras',
                                        'candidate_select','candidate_letter','from_landing')
        ''')
        interviewd_candidates = s.select(f'''select distinct c.fullname 
                                    from "Events" e, "Candidates"  c
                                    where  
                                        e.vacancy_id =  "{v[8]}"
                                        and c.id = e.candidate_id
                                        and e.type_id in ('interview',  'phone_interview',
                                        'group_interview','candidate_phone_call')
        ''')
        interviewd_rr = s.select(f'''select distinct c.fullname 
                                            from "Events" e, "Candidates"  c
                                            where  
                                                e.vacancy_id =  "{v[8]}"
                                                and c.id = e.candidate_id
                                                and e.type_id in ('rr_interview',  'rr_interview2', 'event_type_1')
                ''')
        final = s.select(f'''select distinct c.fullname 
                                                    from "Events" e, "Candidates"  c
                                                    where  
                                                        e.vacancy_id =  "{v[8]}"
                                                        and c.id = e.candidate_id
                                                        and e.type_id in ('job_offer' )
                        ''')
        data.append([
            v[0],  # Наименование вакансии
            v[7],  # ЦФО
            "https://hh.ru/vacancy/" + v[5][3:] if v[5] != '' else '',  # Ссылка на размещение
            s.vacancy_priority[v[13]],  # Срочность вакансии
            s.vacancy_difficulty_level[v[12]],  # Сложность вакансии
            s.staff_category[v[11]],  # Категория вакансии
            f'=HYPERLINK("{"https://hh.ru/vacancy/" + v[5][3:]}";"{v[0]}")' if v[5] != '' else v[0],
            # Наименование со ссылкой
            v[6],  # Заявитель
            v[2][:10],  # Дата заявки
            v[9],  # Ответственный рекрутер
            s.vacancy_status[v[4]],  # status
            '',  # Дней в работе
            v[3][:10] if v[3] != '' else '',  # Дата закрытия
            f'''Всего кандидатов:{c[0][0]},\n{st}
            ''',  # Детальная информация по подбору #13
            f'Всего: {len(vacancy_response)}\n\t' + '\n\t'.join([i for sub in vacancy_response for i in sub]),
            # Отклики разобрано  vacancy_response
            f'Всего: {len(interviewd_candidates)}\n\t' + '\n \t'.join(
                [i for sub in interviewd_candidates for i in sub]),
            # Интервью с рекрутером
            f'Всего: {len(interviewd_rr)}\n\t' + '\n\t'.join([i for sub in interviewd_rr for i in sub]),
            # Интервью с руководителем interviewd_rr
            f'Всего: {len(final)}\n\t' + '\n\t'.join([i for sub in final for i in sub]),  # v[10],#Финалисты
        ])
        formula[0].append([f'=ГИПЕРССЫЛКА(C{r};A{r})'])
        formula[1].append([f'=ОКРУГЛ(ЕСЛИ(M{r}="";ТДАТА()-I{r};M{r}-I{r}))'])
        r += 1

    gw.ws.append_rows(data)
    gw.ws.update(values=formula[0], range_name='G2:G' + (len(formula[0]) + 1).__str__(), raw=False)
    gw.ws.update(values=formula[1], range_name='L2:L' + (len(formula[1]) + 1).__str__(), raw=False)
    gw.ws.format('L2:L300', {'numberFormat': {'type': 'text', 'pattern': '##0'}})

    pass

#0 - отчет за все время
#1 - отчет за текущий год
#2 - отчет за текущий квартал
#3 - отчет за текущий месяц
#4 - отчет за текущую неделю

def report_by_period(gw=None,s=None, period=0):

    if period==0:
        gw.set_sheet('report')
        start_from = datetime.datetime(2000,1,1)
    elif period==1:
        gw.set_sheet('report (годовой)')
        start_from=datetime.datetime(datetime.datetime.now().year,1,1)
    elif period==2:
        gw.set_sheet('report (квартал)')
        q = ((datetime.datetime.now().month+2)//3)-1
        start_from=datetime.datetime(datetime.datetime.now().year, q*3+1,1)
    elif period==3:
        gw.set_sheet('report (месяц)')
        start_from=datetime.datetime(datetime.datetime.now().year,datetime.datetime.now().month,1)
    elif period==4:
        gw.set_sheet('report (неделя)')
        start_from=datetime.datetime.today() - datetime.timedelta(days=datetime.datetime.today().weekday())

    vacancy = s.select(sql='''select
                                v.name as "Vacancy", v.code, v.start_date, v.close_date, v.state_id,
                                v.inet_uid as "HH", v.rr_persons_desc as "Requestor", d.name as "Dep", 
                                v.id, u.fullname as "Recruter", c.fullname as "FinalCandidate", 
                                v.staff_category_id as 'StaffCat', v.difficulty_level_id as 'Difficulty',
                                v.priority_id as 'Priority'
                            from
                                Vacancies v
                            left join vacancy_instance vi on v.id=vi.vacancy_id
                            left join "Divisions" d on vi.division_id = d.id
                            left join "Users" u on u.id= vi.user_id
                            left join "Candidates" c on c.id= v.final_candidate_id
                            order by v.state_id,v.code desc ''')

    gw.ws.batch_clear(["A2:R200"])

    data = []
    formula = [[], []]
    r = 2
    for v in vacancy[:]:

        c = s.select(f'''select count(*) 
                            from (
                                select distinct e.candidate_id 
                                from "Events" e
                                where  e.vacancy_id = "{v[8]}"
                                and e."date" > '{start_from}') ''')

        stat = s.select(f''' select e.type_id, count(e.id)  
                            from "Events" e
                            where  e.vacancy_id = "{v[8]}"
                            and e."date" > '{start_from}'
                            group by e.type_id
                        ''')
        st = "Статистика по событиям:\n"

        for row in stat:
            st += '\t' + s.event_types[row[0]] + ':' + str(row[1]) + ',\n'
        vacancy_response = s.select(f'''select distinct c.fullname 
                                    from "Events" e, "Candidates"  c
                                    where  
                                        e.vacancy_id =  "{v[8]}"
                                        and c.id = e.candidate_id
                                        and e.type_id in ('vacancy_response',  'found_with_ras',
                                        'candidate_select','candidate_letter','from_landing')
                                        and e."date" > '{start_from}'
        ''')
        interviewd_candidates = s.select(f'''select distinct c.fullname 
                                    from "Events" e, "Candidates"  c
                                    where  
                                        e.vacancy_id =  "{v[8]}"
                                        and c.id = e.candidate_id
                                        and e.type_id in ('interview',  'phone_interview',
                                        'group_interview','candidate_phone_call')
                                        and e."date" > '{start_from}'
        ''')
        interviewd_rr = s.select(f'''select distinct c.fullname 
                                            from "Events" e, "Candidates"  c
                                            where  
                                                e.vacancy_id =  "{v[8]}"
                                                and c.id = e.candidate_id
                                                and e.type_id in ('rr_interview',  'rr_interview2', 'event_type_1')
                                                and e."date" > '{start_from}'
                ''')

        final = s.select(f'''select distinct c.fullname 
                                                    from "Events" e, "Candidates"  c
                                                    where  
                                                        e.vacancy_id =  "{v[8]}"
                                                        and c.id = e.candidate_id
                                                        and e.type_id in ('job_offer' )
                                                        and e."date" > '{start_from}'
                        ''')
        data.append([
            v[0],  # Наименование вакансии
            v[7],  # ЦФО
            "https://hh.ru/vacancy/" + v[5][3:] if v[5] != '' else '',  # Ссылка на размещение
            s.vacancy_priority[v[13]],  # Срочность вакансии
            s.vacancy_difficulty_level[v[12]],  # Сложность вакансии
            s.staff_category[v[11]],  # Категория вакансии
            f'=HYPERLINK("{"https://hh.ru/vacancy/" + v[5][3:]}";"{v[0]}")' if v[5] != '' else v[0],
            # Наименование со ссылкой
            v[6],  # Заявитель
            v[2][:10],  # Дата заявки
            v[9],  # Ответственный рекрутер
            s.vacancy_status[v[4]],  # status
            '',  # Дней в работе
            v[3][:10] if v[3] != '' else '',  # Дата закрытия
            f'''Всего кандидатов:{c[0][0]},\n{st}
            ''',  # Детальная информация по подбору #13
            f'Всего: {len(vacancy_response)}\n\t' + '\n\t'.join([i for sub in vacancy_response for i in sub]),
            # Отклики разобрано  vacancy_response
            f'Всего: {len(interviewd_candidates)}\n\t' + '\n \t'.join(
                [i for sub in interviewd_candidates for i in sub]),
            # Интервью с рекрутером
            f'Всего: {len(interviewd_rr)}\n\t' + '\n\t'.join([i for sub in interviewd_rr for i in sub]),
            # Интервью с руководителем interviewd_rr
            f'Всего: {len(final)}\n\t' + '\n\t'.join([i for sub in final for i in sub]),  # v[10],#Финалисты
        ])
        formula[0].append([f'=ГИПЕРССЫЛКА(C{r};A{r})'])
        formula[1].append([f'=ОКРУГЛ(ЕСЛИ(M{r}="";ТДАТА()-I{r};M{r}-I{r}))'])
        r += 1

    gw.ws.append_rows(data)
    gw.ws.update(values=formula[0], range_name='G2:G' + (len(formula[0]) + 1).__str__(), raw=False)
    gw.ws.update(values=formula[1], range_name='L2:L' + (len(formula[1]) + 1).__str__(), raw=False)
    gw.ws.format('L2:L300', {'numberFormat': {'type': 'text', 'pattern': '##0'}})

    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-DB', "--DBPATH", action='store', help='Database path', default="estaff_report.db ")
    subparsers = parser.add_subparsers(help='List of commands', required=True)
    load_parser = subparsers.add_parser('load', help='Load Estaff files')
    load_parser.add_argument('dirname', action='store', help='Directory to load',
                             default="C:\\Program Files\\EStaff_Server\\data_rcr\\obj\\")
    load_parser.set_defaults(func=loader)
    report_parser = subparsers.add_parser('report', help='save report to gdrive')
    report_parser.add_argument('key', action='store', help='key file')
    report_parser.add_argument('gdoc', action='store', help='gdoc id')
    report_parser.set_defaults(func=report)
    args = parser.parse_args()
    args.func(args)
